import Gallery from './Gallery.js';
import { createRoot } from 'react-dom/client';
import App from './App.js';
import { StrictMode } from 'react';
import Cpp from './Cpp';
import Spp from './Spp.js';
import Ppp from './Ppp.js';
import Dpp from './Dpp.js';
import Rpp from './Rpp.js';
import Gpp from './Gpp.js';
import AppA from './AppA.js';

const root = createRoot(document.getElementById('root'));
root.render(
  <StrictMode>
    <div>
      <Rpp />
    </div>
    <Dpp />
    <Cpp />
    <Spp />
    <App />
    <Gallery />
    {/* <Ppp /> */}
    <Gpp />
    <AppA />
    {/* <Ppp /> */}
  </StrictMode>,
);
